package fpt.ph.my.tracker.config;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.DriverManagerDataSource;

/**
 *
 * @author Patrick Tan
 */
@Configuration
@EnableTransactionManagement
public class HibernateConfig {

    private static final Logger LOGGER = Logger.getLogger(HibernateConfig.class);
    @Autowired
    private Environment env;

    @Bean(name = "sessionFactory")
    public SessionFactory sessionFactory() {
        LOGGER.info("Initializing sessionFactory!");

        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
        builder.scanPackages("fpt.ph.*").addProperties(getHibernateProperties());
        return builder.buildSessionFactory();
    }

    private Properties getHibernateProperties() {
        Properties prop = new Properties();

        prop.put("hibernate.format_sql", "true");
        prop.put("hibernate.show_sql", "true");
        prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

        return prop;
    }

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClass("com.mysql.jdbc.Driver");

        ds.setJdbcUrl(env.getProperty("database.url"));
        ds.setUser(env.getProperty("database.username"));
        ds.setPassword(env.getProperty("database.password"));

        return ds;
    }

    @Bean
    public HibernateTransactionManager txManager() {
        return new HibernateTransactionManager(sessionFactory());
    }
}
