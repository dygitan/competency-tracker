package fpt.ph.my.tracker.config;

import fpt.ph.my.tracker.config.security.WebSecurityConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

/**
 *
 * @author Patrick Tan
 */
@EnableWebMvc
@Configuration
@ComponentScan({"fpt.ph.*"})
@PropertySource("classpath:application.properties")
@Import({HibernateConfig.class, WebSecurityConfig.class})
public class SpringConfig {

    @Bean
    public TilesConfigurer tileConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions("/WEB-INF/tiles/*.xml");
        return tilesConfigurer;
    }

    @Bean
    public TilesViewResolver viewResolver() {
        TilesViewResolver resolver = new TilesViewResolver();
        return resolver;
    }

//    @Bean
//    public InternalResourceViewResolver viewResolver() {
//        LOGGER.info("Initializing: viewResolver() ...");
//
//        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//        viewResolver.setViewClass(JstlView.class);
//        viewResolver.setPrefix("/WEB-INF/pages/");
//        viewResolver.setSuffix(".jsp");
//        return viewResolver;
//    }
}
