package fpt.ph.my.tracker.config;

import java.lang.reflect.Field;

/**
 *
 * @author Patrick Tan
 */
public class PrinterUtil {

    public static String printMe(Object o) {
        if (o == null) {
            return null;
        }

        Field[] fields = o.getClass().getSuperclass().getDeclaredFields();

        StringBuilder sb = new StringBuilder();

        for (Field field : fields) {
            field.setAccessible(true);

            try {
                Object value = field.get(o);

                sb.append("[");
                sb.append(field.getName());
                sb.append("=");
                sb.append(value);
                sb.append("]");

            } catch (IllegalArgumentException | IllegalAccessException ex) {
                ex.printStackTrace(System.err);
            }
        }

        return sb.toString();
    }
}
