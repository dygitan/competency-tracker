package fpt.ph.my.tracker.model;
// Generated Aug 13, 2015 6:44:41 PM by Hibernate Tools 4.3.1

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fpt.ph.my.tracker.config.PrinterUtil;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * CompetencyCategory generated by hbm2java
 */
@Entity
@Table(name = "competency_category")
@AttributeOverride(name = "primaryId", column = @Column(name = "category_id"))
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@uuid")
public class CompetencyCategory extends BaseModel implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private String description;
    private Set<Competency> competencies = new HashSet<>(0);

    public CompetencyCategory() {
    }

    @Column(name = "description", length = 50)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "competencyCategory")
    public Set<Competency> getCompetencies() {
        return this.competencies;
    }

    public void setCompetencies(Set<Competency> competencies) {
        this.competencies = competencies;
    }

    @Override
    public String toString() {
        return PrinterUtil.printMe(this);
    }

}
