package fpt.ph.my.tracker.model.custom;

import javax.validation.constraints.AssertTrue;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.StringUtils;

/**
 *
 * @author Patrick Tan
 */
public class DeveloperRegistration {

    @NotEmpty
    private String lastName;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
    private String confirmPassword;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @AssertTrue(message = "the password entered did not matched.")
    public boolean isValid() {
        if (StringUtils.isEmpty(password)) {
            return !StringUtils.isEmpty(password) && password.equals(confirmPassword);
        } else if (StringUtils.isEmpty(password) && StringUtils.isEmpty(confirmPassword)) {
            return true;
        } else {
            return !StringUtils.isEmpty(password) && password.equals(confirmPassword);
        }
    }
}
