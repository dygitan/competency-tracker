package fpt.ph.my.tracker.model.custom;

/**
 *
 * @author Patrick Tan
 */
public class CompetencyCategoryRating {

    private Integer categoryId;
    private String description;
    private Double rating;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

}
