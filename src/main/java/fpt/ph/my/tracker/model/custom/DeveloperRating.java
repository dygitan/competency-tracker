package fpt.ph.my.tracker.model.custom;

import fpt.ph.my.tracker.util.AppUtil;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Patrick Tan
 */
public class DeveloperRating {

    private Integer developerId;
    private String lastName;
    private String firstName;
    private BigDecimal rating;
    private Date coveredPeriodStart;
    private Date coveredPeriodEnd;

    public Integer getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(Integer developerId) {
        this.developerId = developerId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public BigDecimal getRating() {
        return rating;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    @Column(name = "covered_period_start")
    public Date getCoveredPeriodStart() {
        return AppUtil.clone(coveredPeriodStart);
    }

    public void setCoveredPeriodStart(Date coveredPeriodStart) {
        this.coveredPeriodStart = AppUtil.clone(coveredPeriodStart);
    }

    @Column(name = "covered_period_end")
    public Date getCoveredPeriodEnd() {
        return AppUtil.clone(coveredPeriodEnd);
    }

    public void setCoveredPeriodEnd(Date coveredPeriodEnd) {
        this.coveredPeriodEnd = AppUtil.clone(coveredPeriodEnd);
    }

}
