package fpt.ph.my.tracker.controller.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Patrick Tan
 */
@Controller
public class HomeController {

    @RequestMapping("/public/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/public/register")
    public String register() {
        return "register";
    }

    @RequestMapping("/secured/dashboard")
    public String dashboard() {
        return "dashboard";
    }

    @RequestMapping("/*")
    public String index() {
        return "redirect:secured/dashboard";
    }

    @RequestMapping("/restricted")
    public String restricted() {
        return "restricted";
    }
}
