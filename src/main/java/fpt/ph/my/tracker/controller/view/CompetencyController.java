package fpt.ph.my.tracker.controller.view;

import fpt.ph.my.tracker.service.CategoryService;
import fpt.ph.my.tracker.service.CompetencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Patrick Tan
 */
@Component
@RequestMapping("/secured/competency/*")
public class CompetencyController {

    @Autowired
    @Qualifier("categoryServiceImpl")
    private CategoryService categoryService;
    @Autowired
    @Qualifier("competencyServiceImpl")
    private CompetencyService competencyService;

    @RequestMapping("all")
    public String showAllCompetencies() {
        return "showAllCompetencies";
    }

    @RequestMapping("new")
    public String newCompetency(ModelMap model) {
        model.addAttribute("categories", categoryService.getAll(0));
        return "viewCompetency";
    }

    @RequestMapping("{id}")
    public String viewCompetency(@PathVariable("id") Integer categoryId, ModelMap model) {
        model.addAttribute("categories", categoryService.getAll(0));
        model.addAttribute("competency", competencyService.get(categoryId));
        return "viewCompetency";
    }

}
