package fpt.ph.my.tracker.controller.rest;

import fpt.ph.my.tracker.model.Competency;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import fpt.ph.my.tracker.service.CompetencyService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Patrick Tan
 */
@RestController
@RequestMapping("/rest/api/competency/*")
public class CompetencyRestController {

    @Autowired
    @Qualifier("competencyServiceImpl")
    private CompetencyService competencyService;

    @RequestMapping("get/all")
    public List<Competency> getAllCompetencies() {
        return competencyService.getAll(0);
    }

    @RequestMapping("get/category/{categoryId}")
    public List<Competency> getCompetenciesByCategory(@PathVariable Integer categoryId) {
        return competencyService.getByCategory(categoryId);
    }

    @RequestMapping("get/developer/{developerId}/{competencyId}")
    public List<DeveloperCompetency> getCompetenciesByDev(@PathVariable Integer developerId, @PathVariable Integer competencyId) {
        return competencyService.getByDeveloper(developerId, competencyId);
    }

    @RequestMapping("submit")
    public void submitCompetency(@RequestBody Competency competency) {
        competencyService.saveOrUpdate(competency);
    }

}
