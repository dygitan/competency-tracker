package fpt.ph.my.tracker.controller.rest;

import fpt.ph.my.tracker.model.CompetencyCategory;
import fpt.ph.my.tracker.model.custom.CompetencyCategoryRating;
import fpt.ph.my.tracker.service.CategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Patrick Tan
 */
@RestController
@RequestMapping("/rest/api/category/*")
public class CategoryRestController {

    @Autowired
    @Qualifier("categoryServiceImpl")
    private CategoryService categoryService;

    @RequestMapping("all")
    public List<CompetencyCategory> getAllCategories() {
        return categoryService.getAll(0);
    }

    @RequestMapping("{id}")
    public List<CompetencyCategoryRating> getAllCategories(@PathVariable("id") Integer developerId) {
        return categoryService.getCompetencyCategoriesRating(developerId);
    }

    @RequestMapping("submit")
    public void submitCategory(@RequestBody CompetencyCategory category) {
        categoryService.saveOrUpdate(category);
    }

    @RequestMapping("hello")
    public List<CompetencyCategory> hello() {
        return categoryService.hello();
    }
}
