package fpt.ph.my.tracker.controller.view;

import fpt.ph.my.tracker.model.Developer;
import fpt.ph.my.tracker.service.DeveloperService;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Patrick Tan
 */
@Controller
@RequestMapping("/secured/developer/*")
public class DeveloperController {

    private static final Logger LOGGER = Logger.getLogger(DeveloperController.class);

    @Autowired
    @Qualifier("developerServiceImpl")
    private DeveloperService developerService;

    @RequestMapping("all")
    public String showAllDevs() {
        return "showAllDevs";
    }

    @RequestMapping("competency/history/{id}")
    public String showCompetencyHistory(@PathVariable Integer id, ModelMap model) {
        model.addAttribute("developer", developerService.get(id));
        return "showCompetencyHistory";
    }

    @RequestMapping("{id}")
    public String recordCompetencies(@PathVariable Integer id, ModelMap model) {
        model.addAttribute("developer", developerService.get(id));
        return "recordCompetencies";
    }

    @RequestMapping("load/{id}")
    @ResponseBody
    public Developer load(@PathVariable Integer id) {
        return developerService.load(id);
    }

    @RequestMapping("get/{id}")
    @ResponseBody
    public Developer get(@PathVariable Integer id) {
        return developerService.get(id);
    }

    @RequestMapping("export/data")
    public void exportToExcel(HttpServletRequest request, HttpServletResponse response) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM/dd/yyyy");

        try {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=export.xls");

            LOGGER.info(" >>> " + request.getParameter("s"));

            developerService.exportExcel(response.getOutputStream(),
                    sdf.parse(request.getParameter("s")),
                    sdf.parse(request.getParameter("e")));
            
            LOGGER.info("Done");

        } catch (IOException | ParseException e) {
            LOGGER.error(e);
        }
    }

    @RequestMapping("export")
    public String export() {
        return "exportData";
    }

}
