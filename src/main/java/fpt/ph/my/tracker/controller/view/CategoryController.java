package fpt.ph.my.tracker.controller.view;

import fpt.ph.my.tracker.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Patrick Tan
 */
@Controller
@RequestMapping("/secured/category/*")
public class CategoryController {

    @Autowired
    @Qualifier("categoryServiceImpl")
    private CategoryService categoryService;

    @RequestMapping("all")
    public String showAllCategories() {
        return "showAllCategories";
    }

    @RequestMapping("new")
    public String newCategory() {
        return "viewCategory";
    }

    @RequestMapping("{id}")
    public String viewCategory(@PathVariable("id") Integer categoryId, ModelMap model) {
        model.addAttribute("category", categoryService.get(categoryId));
        return "viewCategory";
    }
}
