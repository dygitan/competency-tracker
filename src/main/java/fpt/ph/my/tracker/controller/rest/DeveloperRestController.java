package fpt.ph.my.tracker.controller.rest;

import fpt.ph.my.tracker.model.Developer;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import fpt.ph.my.tracker.model.custom.DeveloperRating;
import fpt.ph.my.tracker.model.custom.DeveloperRegistration;
import fpt.ph.my.tracker.service.CompetencyService;
import fpt.ph.my.tracker.service.DeveloperService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Patrick Tan
 */
@RestController
@RequestMapping("/rest/api/developer/*")
public class DeveloperRestController {

    @Autowired
    @Qualifier("competencyServiceImpl")
    private CompetencyService competencyService;
    @Autowired
    @Qualifier("developerServiceImpl")
    private DeveloperService developerService;

    @RequestMapping("register")
    public List<FieldError> register(@RequestBody @Validated DeveloperRegistration developerRegistration, BindingResult binding) {
        if (!binding.hasErrors()) {
            developerService.register(developerRegistration);
            return null;
        } else {
            return binding.getFieldErrors();
        }
    }

    @RequestMapping("get/all/{offset}")
    public List<DeveloperRating> retrieveAll(@PathVariable Integer offset,
            @RequestParam(value = "s") Date coveredPeriondStart,
            @RequestParam(value = "e") Date coveredPeriodEnd) {
        return developerService.getAllDeveloperCompetencies(coveredPeriondStart, coveredPeriodEnd);
    }

    @RequestMapping("{id}")
    public Developer retrieveByPrimaryId(@PathVariable("id") Integer primaryId) {
        return developerService.get(primaryId);
    }

    @RequestMapping("competency/submit")
    public List<DeveloperCompetency> submitCompetency(@RequestBody DeveloperCompetency developerCompetency) {
        List<DeveloperCompetency> developerCompetencies = new ArrayList<>();
        developerCompetencies.add(developerCompetency);

        developerService.submitCompetencies(developerCompetencies);

        return competencyService.getByDeveloper(developerCompetency.getDeveloper().getPrimaryId(),
                developerCompetency.getCompetency().getPrimaryId());
    }

}
