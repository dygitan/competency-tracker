package fpt.ph.my.tracker.util;

import java.lang.reflect.Field;
import java.util.Date;

/**
 *
 * @author Patrick Tan
 */
public class AppUtil {

    public static Date clone(Date date) {
        if (date == null) {
            return null;
        }

        return (Date) date.clone();
    }

    public static String toString(Object o) {
        if (o == null) {
            return null;
        }

        Field[] fields = o.getClass().getDeclaredFields();

        StringBuilder sb = new StringBuilder();

        for (Field field : fields) {
            field.setAccessible(true);

            try {
                Object value = field.get(o);

                sb.append("[");
                sb.append(field.getName());
                sb.append("=");
                sb.append(value);
                sb.append("]");

            } catch (IllegalArgumentException | IllegalAccessException ex) {
                ex.printStackTrace(System.err);
            }
        }

        return sb.toString();
    }
}
