package fpt.ph.my.tracker.query;

/**
 *
 * @author Patrick Tan
 */
public class QueryCondition {

    public enum ConditionType {

        EQUAL, NOT_EQUAL;
    }

    private String property;
    private Object value1;
    private Object value2;
    private ConditionType conditionType;

    public QueryCondition() {
    }

    public static QueryCondition eq(String property, Object value) {
        QueryCondition condition = new QueryCondition();
        condition.setConditionType(ConditionType.EQUAL);
        condition.setProperty(property);
        condition.setValue1(value);
        return condition;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Object getValue1() {
        return value1;
    }

    public void setValue1(Object value1) {
        this.value1 = value1;
    }

    public Object getValue2() {
        return value2;
    }

    public void setValue2(Object value2) {
        this.value2 = value2;
    }

    public ConditionType getConditionType() {
        return conditionType;
    }

    public void setConditionType(ConditionType conditionType) {
        this.conditionType = conditionType;
    }
}
