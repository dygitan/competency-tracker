package fpt.ph.my.tracker.query;

import org.hibernate.FetchMode;
import org.hibernate.sql.JoinType;

/**
 *
 * @author Patrick Tan
 */
public class QueryCriteria {

    public enum QueryType {

        JOIN, FETCH;
    }

    private String property;
    private String alias;
    private QueryType queryType;
    private JoinType joinType = JoinType.INNER_JOIN;
    private FetchMode fetchMode = FetchMode.JOIN;

    public static QueryCriteria join(String property, String alias) {
        QueryCriteria criteria = new QueryCriteria();
        criteria.setAlias(alias);
        criteria.setProperty(property);
        criteria.setQueryType(QueryType.JOIN);
        return criteria;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

    public JoinType getJoinType() {
        return joinType;
    }

    public void setJoinType(JoinType joinType) {
        this.joinType = joinType;
    }

    public FetchMode getFetchMode() {
        return fetchMode;
    }

    public void setFetchMode(FetchMode fetchMode) {
        this.fetchMode = fetchMode;
    }

}
