package fpt.ph.my.tracker.query;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Patrick Tan
 */
@Component
public class QueryBuilder {

    @Autowired
    private SessionFactory sessionFactory;

    public Criteria buildQuery(QueryObject queryObject) {
        Criteria criteria = getSession().createCriteria(queryObject.getClazz());
        buildCriteria(criteria, queryObject.getCriterias());
        buildCondition(criteria, queryObject.getConditions());
        return criteria;
    }

    private void buildCriteria(Criteria criteria, List<QueryCriteria> qCriterias) {
        for (QueryCriteria qCriteria : qCriterias) {
            switch (qCriteria.getQueryType()) {
                case FETCH:
                    criteria.setFetchMode(qCriteria.getProperty(), FetchMode.JOIN);
                    break;
                case JOIN:
                    criteria.createAlias(qCriteria.getProperty(), qCriteria.getAlias(),
                            qCriteria.getJoinType());
                    break;
                default:
                    throw new IllegalArgumentException("Unknown query type [" + qCriteria.getQueryType() + "]");
            }
        }
    }

    private void buildCondition(Criteria criteria, List<QueryCondition> qConditions) {
        for (QueryCondition qCondition : qConditions) {
            switch (qCondition.getConditionType()) {
                case EQUAL:
                    criteria.add(Restrictions.eq(qCondition.getProperty(), qCondition.getValue1()));
                    break;
                default:
                    throw new IllegalArgumentException("Unknown query condition type [" + qCondition.getConditionType() + "]");
            }
        }
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
