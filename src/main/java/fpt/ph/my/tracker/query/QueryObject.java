package fpt.ph.my.tracker.query;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Patrick Tan
 */
public class QueryObject {

    private Class<?> clazz;
    private List<QueryCriteria> criterias = new ArrayList<>();
    private List<QueryCondition> conditions = new ArrayList<>();

    public QueryObject(Class<?> clazz) {
        this.clazz = clazz;
    }

    public void addCriteria(QueryCriteria criteria) {
        criterias.add(criteria);
    }

    public void addCondition(QueryCondition condition) {
        conditions.add(condition);
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public List<QueryCriteria> getCriterias() {
        return criterias;
    }

    public void setCriterias(List<QueryCriteria> criterias) {
        this.criterias = criterias;
    }

    public List<QueryCondition> getConditions() {
        return conditions;
    }

    public void setConditions(List<QueryCondition> conditions) {
        this.conditions = conditions;
    }

}
