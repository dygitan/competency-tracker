package fpt.ph.my.tracker.service;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 * @param <T>
 */
public interface CommonService<T> {

    @Transactional(rollbackFor = Exception.class)
    void save(T entity);

    @Transactional(rollbackFor = Exception.class)
    void saveOrUpdate(T entity);

    @Transactional(rollbackFor = Exception.class)
    void update(T entity);

}
