package fpt.ph.my.tracker.service;

import fpt.ph.my.tracker.model.Developer;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import fpt.ph.my.tracker.model.custom.DeveloperRating;
import fpt.ph.my.tracker.model.custom.DeveloperRegistration;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 */
public interface DeveloperService extends CommonService<Developer>, ReadService<Developer> {

    @Transactional(readOnly = true)
    void exportExcel(OutputStream out, Date coveredPeriodStart, Date coveredPeriodEnd) throws IOException;

    @Transactional(rollbackFor = Exception.class)
    void register(DeveloperRegistration developerRegistration);

    @Transactional(rollbackFor = Exception.class)
    void submitCompetencies(List<DeveloperCompetency> developerCompetencies);

    @Transactional(readOnly = true)
    List<DeveloperRating> getAllDeveloperCompetencies();

    @Transactional(readOnly = true)
    List<DeveloperRating> getAllDeveloperCompetencies(Date coveredPeriodStart, Date coveredPeriodEnd);
}
