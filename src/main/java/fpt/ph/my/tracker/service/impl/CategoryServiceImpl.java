package fpt.ph.my.tracker.service.impl;

import fpt.ph.my.tracker.dao.CategoryDao;
import fpt.ph.my.tracker.model.CompetencyCategory;
import fpt.ph.my.tracker.model.custom.CompetencyCategoryRating;
import fpt.ph.my.tracker.service.CategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    @Qualifier("categoryDaoImpl")
    private CategoryDao categoryDao;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(CompetencyCategory entity) {
        categoryDao.save(entity);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrUpdate(CompetencyCategory entity) {
        categoryDao.saveOrUpdate(entity);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(CompetencyCategory entity) {
        categoryDao.update(entity);
    }

    @Transactional(readOnly = true)
    @Override
    public CompetencyCategory get(Integer primaryId) {
        return categoryDao.get(primaryId);
    }

    @Transactional(readOnly = true)
    @Override
    public CompetencyCategory getDetailed(Integer primaryId) {
        return categoryDao.getDetails(primaryId);
    }

    @Transactional(readOnly = true)
    @Override
    public CompetencyCategory load(Integer primaryId) {
        return categoryDao.load(primaryId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<CompetencyCategory> getAll(int offset) {
        return categoryDao.getAll(offset);
    }

    @Transactional(readOnly = true)
    @Override
    public List<CompetencyCategoryRating> getCompetencyCategoriesRating(Integer developerId) {
        return categoryDao.getCompetencyCategoriesRating(developerId);
    }
    
    @Override
    public List<CompetencyCategory> hello() {
        return categoryDao.getAll(0);
    }

}
