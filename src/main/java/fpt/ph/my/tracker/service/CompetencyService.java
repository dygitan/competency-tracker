package fpt.ph.my.tracker.service;

import fpt.ph.my.tracker.model.Competency;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 */
public interface CompetencyService extends CommonService<Competency>, ReadService<Competency> {

    @Transactional(readOnly = true)
    List<Competency> getByCategory(Integer categoryId);

    @Transactional(readOnly = true)
    List<DeveloperCompetency> getByDeveloper(Integer developerId, Integer competencyId);

}
