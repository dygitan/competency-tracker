package fpt.ph.my.tracker.service.impl;

import fpt.ph.my.tracker.dao.CommonDao;
import fpt.ph.my.tracker.dao.DeveloperDao;
import fpt.ph.my.tracker.model.Developer;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import fpt.ph.my.tracker.model.Role;
import fpt.ph.my.tracker.model.SecUser;
import fpt.ph.my.tracker.model.SecUserRole;
import fpt.ph.my.tracker.model.custom.DeveloperRating;
import fpt.ph.my.tracker.model.custom.DeveloperRegistration;
import fpt.ph.my.tracker.service.DeveloperService;
import fpt.ph.my.tracker.util.SecurityUtility;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Patrick Tan
 */
@Transactional
@Service
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    @Qualifier("developerDaoImpl")
    private DeveloperDao developerDao;
    @Autowired
    @Qualifier("commonDaoImpl")
    private CommonDao commonDao;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM/dd/yyyy");

    @Override
    public void exportExcel(OutputStream out, Date coveredPeriodStart, Date coveredPeriodEnd) throws IOException {
        List<Developer> developers = developerDao.getAll(coveredPeriodStart, coveredPeriodEnd);

        HSSFWorkbook workbook = new HSSFWorkbook();

        for (Developer developer : developers) {
            HSSFSheet sheet = workbook.createSheet(developer.getFullName());

            HSSFRow headerRow = sheet.createRow(0);

            int index = 0;
            headerRow.createCell(index).setCellValue("Category");
            headerRow.createCell(++index).setCellValue("Competency");
            headerRow.createCell(++index).setCellValue("Period Covered - From");
            headerRow.createCell(++index).setCellValue("Period Covered - To");
            headerRow.createCell(++index).setCellValue("Rating");

            if (!CollectionUtils.isEmpty(developer.getDeveloperCompetencies())) {
                List<DeveloperCompetency> temp = new ArrayList<>(developer.getDeveloperCompetencies());

                Collections.sort(temp, (DeveloperCompetency o1, DeveloperCompetency o2)
                        -> o2.getCoveredPeriodStart().compareTo(o1.getCoveredPeriodStart()));

                int rowCtr = 1;

                for (DeveloperCompetency c : temp) {
                    HSSFRow row = sheet.createRow(rowCtr);
                    int indexX = 0;

                    row.createCell(indexX).setCellValue(c.getCompetency().getCompetencyCategory().getDescription());
                    row.createCell(++indexX).setCellValue(c.getCompetency().getDescription());
                    row.createCell(++indexX).setCellValue(sdf.format(c.getCoveredPeriodStart()));
                    row.createCell(++indexX).setCellValue(sdf.format(c.getCoveredPeriodEnd()));
                    row.createCell(++indexX).setCellValue(c.getRating());

                    rowCtr++;
                }

            }
        }

        workbook.write(out);
        out.close();
    }

    @Override
    public void save(Developer entity) {
        developerDao.save(entity);
    }

    @Override
    public void saveOrUpdate(Developer entity) {
        developerDao.saveOrUpdate(entity);
    }

    @Override
    public void update(Developer entity) {
        developerDao.update(entity);
    }

    @Override
    public void register(DeveloperRegistration developerRegistration) {
        SecUser secUser = new SecUser(developerRegistration.getUsername());
        secUser.setPassword(SecurityUtility.encryptPassword(developerRegistration.getPassword()));
        secUser.setEnabled(Boolean.TRUE);
        commonDao.save(secUser);

        SecUserRole secUserRole = new SecUserRole(new Role(2), secUser);
        commonDao.save(secUserRole);

        Developer developer = new Developer();

        developer.setSecUser(secUser);
        developer.setFirstName(developerRegistration.getFirstName());
        developer.setMiddleName(developerRegistration.getFirstName());
        developer.setLastName(developerRegistration.getLastName());

        developerDao.save(developer);
    }

    @Override
    public void submitCompetencies(List<DeveloperCompetency> developerCompetencies) {
        developerCompetencies.stream().forEach((developerCompetency) -> {
            commonDao.saveOrUpdate(developerCompetency);
        });
    }

    @Override
    public Developer get(Integer primaryId) {
        return developerDao.get(primaryId);
    }

    @Override
    public Developer getDetailed(Integer primaryId) {
        return developerDao.getDetails(primaryId);
    }

    @Override
    public Developer load(Integer primaryId) {
        return developerDao.load(primaryId);
    }

    @Override
    public List<Developer> getAll(int offset) {
        return developerDao.getAll(offset);
    }

    @Override
    public List<DeveloperRating> getAllDeveloperCompetencies() {
        return developerDao.getAllDeveloperCompetencies();
    }

    @Override
    public List<DeveloperRating> getAllDeveloperCompetencies(Date coveredPeriodStart, Date coveredPeriodEnd) {
        return developerDao.getAllDeveloperCompetencies(coveredPeriodStart, coveredPeriodEnd);
    }
}
