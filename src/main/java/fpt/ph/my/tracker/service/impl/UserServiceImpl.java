package fpt.ph.my.tracker.service.impl;

import fpt.ph.my.tracker.dao.UserDao;
import fpt.ph.my.tracker.model.SecUser;
import fpt.ph.my.tracker.model.SecUserRole;
import fpt.ph.my.tracker.model.custom.UserBean;
import fpt.ph.my.tracker.service.UserService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Patrick Tan
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    @Qualifier("userDaoImpl")
    private UserDao userDao;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SecUser user = userDao.findByUsername(username);

        UserBean loginUser = new UserBean(user.getUsername(), user.getPassword(),
                user.isEnabled(), true, true, true,
                buildUserAuthority(user.getSecUserRoles()));

        if (!CollectionUtils.isEmpty(user.getDevelopers())) {
            loginUser.setDeveloperId(new ArrayList<>(user.getDevelopers()).get(0).getPrimaryId());
        }

        return loginUser;
    }

    private Set<GrantedAuthority> buildUserAuthority(Set<SecUserRole> userRoles) {
        Set<GrantedAuthority> setAuths = new HashSet();

        userRoles.stream().forEach((userRole) -> {
            setAuths.add(new SimpleGrantedAuthority(userRole.getRole().getRole()));
        });

        return setAuths;
    }
}
