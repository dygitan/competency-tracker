package fpt.ph.my.tracker.service;

import fpt.ph.my.tracker.model.CompetencyCategory;
import fpt.ph.my.tracker.model.custom.CompetencyCategoryRating;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 */
public interface CategoryService extends CommonService<CompetencyCategory>, ReadService<CompetencyCategory> {

    List<CompetencyCategoryRating> getCompetencyCategoriesRating(Integer developerId);

//    @Transactional(readOnly = true)
    List<CompetencyCategory> hello();
}
