package fpt.ph.my.tracker.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 * @param <T>
 */
public interface ReadService<T> {

    @Transactional(readOnly = true)
    T get(Integer primaryId);

    @Transactional(readOnly = true)
    T getDetailed(Integer primaryId);

    @Transactional(readOnly = true)
    T load(Integer primaryId);

    @Transactional(readOnly = true)
    List<T> getAll(int offset);

}
