package fpt.ph.my.tracker.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author Patrick Tan
 */
public interface UserService extends UserDetailsService {

}
