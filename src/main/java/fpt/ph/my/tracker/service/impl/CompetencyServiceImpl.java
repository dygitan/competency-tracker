package fpt.ph.my.tracker.service.impl;

import fpt.ph.my.tracker.dao.CompetencyDao;
import fpt.ph.my.tracker.model.Competency;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import fpt.ph.my.tracker.service.CompetencyService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CompetencyServiceImpl implements CompetencyService {

    @Autowired
    @Qualifier("competencyDaoImpl")
    private CompetencyDao competencyDao;

    @Override
    public void save(Competency entity) {
        competencyDao.save(entity);
    }

    @Override
    public void saveOrUpdate(Competency entity) {
        competencyDao.saveOrUpdate(entity);
    }

    @Override
    public void update(Competency entity) {
        competencyDao.update(entity);
    }

    @Override
    public Competency get(Integer primaryId) {
        return competencyDao.get(primaryId);
    }

    @Override
    public Competency getDetailed(Integer primaryId) {
        return competencyDao.getDetails(primaryId);
    }

    @Override
    public Competency load(Integer primaryId) {
        return competencyDao.load(primaryId);
    }

    @Override
    public List<Competency> getAll(int offset) {
        return competencyDao.getAll(offset);
    }

    @Override
    public List<Competency> getByCategory(Integer categoryId) {
        return competencyDao.getByCategory(categoryId);
    }

    @Override
    public List<DeveloperCompetency> getByDeveloper(Integer developerId, Integer competencyId) {
        return competencyDao.getByDeveloper(developerId, competencyId);
    }

}
