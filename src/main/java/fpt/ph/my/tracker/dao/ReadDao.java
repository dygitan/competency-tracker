/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fpt.ph.my.tracker.dao;

import java.util.List;

/**
 *
 * @author Patrick Tan
 * @param <T>
 */
public interface ReadDao<T> {

    T get(Integer primaryId);

    T getDetails(Integer primaryId);

    T load(Integer primaryId);

    List<T> getAll(int offset);

}
