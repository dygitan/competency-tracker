package fpt.ph.my.tracker.dao;

import fpt.ph.my.tracker.model.Developer;
import fpt.ph.my.tracker.model.custom.DeveloperRating;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Patrick Tan
 */
public interface DeveloperDao extends CommonDao<Developer>, ReadDao<Developer> {

    List<Developer> getAll(Date coveredPeriodStart, Date coveredPeriodEnd);

    List<DeveloperRating> getAllDeveloperCompetencies();

    List<DeveloperRating> getAllDeveloperCompetencies(Date coveredPeriodStart, Date coveredPeriodEnd);
}
