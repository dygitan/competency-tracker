package fpt.ph.my.tracker.dao;

import fpt.ph.my.tracker.model.CompetencyCategory;
import fpt.ph.my.tracker.model.custom.CompetencyCategoryRating;
import java.util.List;

/**
 *
 * @author Patrick Tan
 */
public interface CategoryDao extends CommonDao<CompetencyCategory>,
        ReadDao<CompetencyCategory> {

    List<CompetencyCategoryRating> getCompetencyCategoriesRating(Integer developerId);
}
