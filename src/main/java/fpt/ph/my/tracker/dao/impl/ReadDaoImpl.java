package fpt.ph.my.tracker.dao.impl;

/**
 *
 * @author Patrick Tan
 * @param <T>
 */
public abstract class ReadDaoImpl<T> {

    public abstract T get(Integer primaryId);
}
