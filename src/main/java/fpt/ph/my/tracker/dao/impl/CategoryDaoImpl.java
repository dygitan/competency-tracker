package fpt.ph.my.tracker.dao.impl;

import fpt.ph.my.tracker.dao.CategoryDao;
import fpt.ph.my.tracker.model.CompetencyCategory;
import fpt.ph.my.tracker.model.custom.CompetencyCategoryRating;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 */
@Repository
public class CategoryDaoImpl extends CommonDaoImpl<CompetencyCategory> implements CategoryDao {

    @Override
    public CompetencyCategory get(Integer primaryId) {
        return (CompetencyCategory) getSession().get(CompetencyCategory.class, primaryId);
    }

    @Override
    public CompetencyCategory getDetails(Integer primaryId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CompetencyCategory load(Integer primaryId) {
        return (CompetencyCategory) getSession().load(CompetencyCategory.class, primaryId);
    }

    @Override
    public List<CompetencyCategory> getAll(int offset) {
        Criteria criteria = getSession().createCriteria(CompetencyCategory.class);

        return criteria.list();
    }

    @Override
    public List<CompetencyCategoryRating> getCompetencyCategoriesRating(Integer developerId) {
        Criteria criteria = getSession().createCriteria(CompetencyCategory.class);

        criteria.createAlias("competencies", "competencies", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("competencies.developerCompetencies", "developerCompetencies", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("developerCompetencies.developer", "developer", JoinType.LEFT_OUTER_JOIN);

        ProjectionList projections = Projections.projectionList();

        projections.add(Projections.property("primaryId").as("categoryId"));
        projections.add(Projections.property("description").as("description"));
        projections.add(Projections.avg("developerCompetencies.rating").as("rating"));
        projections.add(Projections.groupProperty("primaryId"));

        criteria.setProjection(projections);

        Disjunction disjunction = Restrictions.disjunction();

        disjunction.add(Restrictions.isNull("developer.primaryId"));
        disjunction.add(Restrictions.isNull("developerCompetencies.primaryId"));
        disjunction.add(Restrictions.eq("developer.primaryId", developerId));

        criteria.add(disjunction);

        criteria.setResultTransformer(Transformers.aliasToBean(CompetencyCategoryRating.class));

        return criteria.list();
    }

}
