package fpt.ph.my.tracker.dao.impl;

import fpt.ph.my.tracker.dao.DeveloperDao;
import fpt.ph.my.tracker.model.Developer;
import fpt.ph.my.tracker.model.custom.DeveloperRating;
import java.util.Date;
import java.util.List;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Filter;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 */
@Repository
public class DeveloperDaoImpl extends CommonDaoImpl<Developer> implements DeveloperDao {

    @Override
    public Developer get(Integer primaryId) {
        getSession().setCacheMode(CacheMode.NORMAL);
        return (Developer) getSession().get(Developer.class, primaryId);
    }

    @Override
    public Developer getDetails(Integer primaryId) {
        Criteria criteria = getSession().createCriteria(Developer.class);

        criteria.createAlias("developerCompetencies", "developerCompetencies", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("developerCompetencies.competency", "competency", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("competency.competencyCategory", "competencyCategory", JoinType.LEFT_OUTER_JOIN);

        criteria.setFetchMode("developerCompetencies", FetchMode.JOIN);
        criteria.add(Restrictions.eq("primaryId", primaryId));

        return (Developer) criteria.uniqueResult();
    }

    @Override
    public Developer load(Integer primaryId) {
        return (Developer) getSession().load(Developer.class, primaryId);
    }

    @Override
    public List<Developer> getAll(int offset) {
        Criteria criteria = getSession().createCriteria(Developer.class);

        criteria.createAlias("developerCompetencies", "developerCompetencies", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("developerCompetencies.competency", "competency", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("competency.competencyCategory", "category", JoinType.LEFT_OUTER_JOIN);

        criteria.addOrder(Order.asc("lastName"));
        criteria.addOrder(Order.asc("firstName"));
        criteria.addOrder(Order.asc("competency.description"));

        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return criteria.list();
    }

    @Override
    public List<Developer> getAll(Date coveredPeriodStart, Date coveredPeriodEnd) {
        Filter filter = getSession().enableFilter("filterByCoveredPeriod");

        filter.setParameter("coveredPeriodStart", coveredPeriodStart);
        filter.setParameter("coveredPeriodEnd", coveredPeriodEnd);

        Criteria criteria = getSession().createCriteria(Developer.class);

        criteria.setFetchMode("developerCompetencies", FetchMode.JOIN);
        criteria.createAlias("developerCompetencies.competency", "competency", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("competency.competencyCategory", "category", JoinType.LEFT_OUTER_JOIN);

        criteria.addOrder(Order.asc("lastName"));
        criteria.addOrder(Order.asc("firstName"));
        criteria.addOrder(Order.asc("category.description"));
        criteria.addOrder(Order.asc("competency.description"));

        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return criteria.list();
    }

    @Override
    public List<DeveloperRating> getAllDeveloperCompetencies() {
        Criteria criteria = getSession().createCriteria(Developer.class);

        criteria.createAlias("developerCompetencies", "developerCompetencies", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("developerCompetencies.competency", "competency", JoinType.LEFT_OUTER_JOIN);

        ProjectionList projections = Projections.projectionList();

        projections.add(Projections.avg("developerCompetencies.rating").as("rating"));
        projections.add(Projections.groupProperty("primaryId"));

        projections.add(Projections.property("primaryId").as("developerId"));
        projections.add(Projections.property("lastName").as("lastName"));
        projections.add(Projections.property("firstName").as("firstName"));
        projections.add(Projections.property("developerCompetencies.coveredPeriodStart").as("coveredPeriodStart"));
        projections.add(Projections.property("developerCompetencies.coveredPeriodEnd").as("coveredPeriodEnd"));

        criteria.setProjection(projections);

        criteria.addOrder(Order.asc("lastName"));
        criteria.addOrder(Order.asc("firstName"));

        criteria.setResultTransformer(Transformers.aliasToBean(DeveloperRating.class));

        return criteria.list();
    }

    @Override
    public List<DeveloperRating> getAllDeveloperCompetencies(Date coveredPeriodStart, Date coveredPeriodEnd) {
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT t1.developer_id as developerId, ");
        sb.append("t1.last_name as lastName, ");
        sb.append("t1.first_name as firstName, ");

        sb.append("(SELECT SUM(rating) FROM developer_competency t2 ");
        sb.append("WHERE t2.developer_id = t1.developer_id ");

        if (coveredPeriodStart != null && coveredPeriodEnd != null) {
            sb.append("AND t2.covered_period_start = :coveredPeriodStart ");
            sb.append("AND t2.covered_period_end = :coveredPeriodEnd ");
        }

        sb.append("GROUP BY developer_id) ");
        sb.append("/ (SELECT COUNT(competency_id) FROM competency) AS rating ");

        sb.append("FROM developer t1 ");
        sb.append("ORDER BY t1.last_name, t1.first_name ASC");

        SQLQuery query = getSession().createSQLQuery(sb.toString());

        if (coveredPeriodStart != null && coveredPeriodEnd != null) {
            query.setParameter("coveredPeriodStart", coveredPeriodStart);
            query.setParameter("coveredPeriodEnd", coveredPeriodEnd);
        }

        query.setResultTransformer(Transformers.aliasToBean(DeveloperRating.class));

        return query.list();
    }

}
