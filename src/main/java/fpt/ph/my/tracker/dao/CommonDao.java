package fpt.ph.my.tracker.dao;

import fpt.ph.my.tracker.query.QueryObject;
import java.util.List;

/**
 *
 * @author Patrick Tan
 * @param <T>
 */
public interface CommonDao<T> {

    void save(T entity);

    void saveOrUpdate(T entity);

    void update(T entity);

    List<T> queryList(QueryObject queryObject);
}
