package fpt.ph.my.tracker.dao.impl;

import fpt.ph.my.tracker.dao.UserDao;
import fpt.ph.my.tracker.model.SecUser;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 */
@Repository
public class UserDaoImpl extends CommonDaoImpl<SecUser> implements UserDao {

    @Override
    public SecUser findByUsername(String username) {
        Criteria criteria = getSession().createCriteria(SecUser.class);
        
        criteria.setFetchMode("secUserRoles", FetchMode.JOIN);
        criteria.setFetchMode("developers", FetchMode.JOIN);
        criteria.add(Restrictions.eq("username", username));

        return (SecUser) criteria.uniqueResult();
    }
}
