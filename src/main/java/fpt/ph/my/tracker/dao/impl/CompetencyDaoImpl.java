package fpt.ph.my.tracker.dao.impl;

import fpt.ph.my.tracker.dao.CompetencyDao;
import fpt.ph.my.tracker.model.Competency;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 */
@Repository
public class CompetencyDaoImpl extends CommonDaoImpl<Competency> implements CompetencyDao {

    @Override
    public Competency get(Integer primaryId) {
        Criteria criteria = getSession().createCriteria(Competency.class);
        criteria.createAlias("competencyCategory", "competencyCategory");
        criteria.add(Restrictions.eq("primaryId", primaryId));
        return (Competency) criteria.uniqueResult();
    }

    @Override
    public Competency getDetails(Integer primaryId) {
        Criteria criteria = getSession().createCriteria(Competency.class);

        return (Competency) criteria.uniqueResult();
    }

    @Override
    public Competency load(Integer primaryId) {
        return (Competency) getSession().load(Competency.class, primaryId);
    }

    @Override
    public List<Competency> getAll(int offset) {
        Criteria criteria = getSession().createCriteria(Competency.class);
        criteria.createAlias("competencyCategory", "competencyCategory");

        criteria.addOrder(Order.asc("competencyCategory.description"));
        criteria.addOrder(Order.asc("description"));

        return criteria.list();
    }

    @Override
    public List<Competency> getByCategory(Integer categoryId) {
        Criteria criteria = getSession().createCriteria(Competency.class);

        criteria.createAlias("competencyCategory", "competencyCategory");
        criteria.add(Restrictions.eq("competencyCategory.primaryId", categoryId));
        criteria.addOrder(Order.asc("description"));

        return criteria.list();
    }

    @Override
    public List<DeveloperCompetency> getByDeveloper(Integer developerId, Integer competencyId) {
        Criteria criteria = getSession().createCriteria(DeveloperCompetency.class);

        criteria.createAlias("competency", "competency");
        criteria.createAlias("developer", "developer");

        criteria.add(Restrictions.eq("competency.primaryId", competencyId));
        criteria.add(Restrictions.eq("developer.primaryId", developerId));

        criteria.addOrder(Order.desc("coveredPeriodStart"));
        criteria.addOrder(Order.asc("competency.description"));

        return criteria.list();
    }

}
