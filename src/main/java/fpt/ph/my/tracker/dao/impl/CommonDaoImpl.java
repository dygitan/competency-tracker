package fpt.ph.my.tracker.dao.impl;

import fpt.ph.my.tracker.dao.CommonDao;
import fpt.ph.my.tracker.query.QueryBuilder;
import fpt.ph.my.tracker.query.QueryObject;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 * @param <T>
 */
@Repository
public class CommonDaoImpl<T> implements CommonDao<T> {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private QueryBuilder queryBuilder;

    @Override
    public void save(T entity) {
        getSession().save(entity);
    }

    @Override
    public void saveOrUpdate(T entity) {
        getSession().saveOrUpdate(entity);
    }

    @Override
    public void update(T entity) {
        getSession().update(entity);
    }

    @Override
    public List<T> queryList(QueryObject queryObject) {
        return queryBuilder.buildQuery(queryObject).list();
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

}
