package fpt.ph.my.tracker.dao;

import fpt.ph.my.tracker.model.Competency;
import fpt.ph.my.tracker.model.DeveloperCompetency;
import java.util.List;

/**
 *
 * @author Patrick Tan
 */
public interface CompetencyDao extends CommonDao<Competency>, ReadDao<Competency> {

    List<Competency> getByCategory(Integer categoryId);

    List<DeveloperCompetency> getByDeveloper(Integer developerId, Integer competencyId);

}
