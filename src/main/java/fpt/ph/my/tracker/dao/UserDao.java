package fpt.ph.my.tracker.dao;

import fpt.ph.my.tracker.model.SecUser;

/**
 *
 * @author Patrick Tan
 */
public interface UserDao extends CommonDao<SecUser> {

    SecUser findByUsername(String username);
}
