/* global angular, baseUrl, csrfToken */

var app = angular.module('registerModule', [
    'registrationCtrl',
    'globalModule'
]);

var registrationCtrl = angular.module('registrationCtrl', []);

registrationCtrl.controller('registerCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        $scope.reset = function () {
            $scope.developer = {};
            $scope.errors = {};
        };

        $scope.register = function () {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var data = $scope.developer;

            if (!data) {
                data = {
                    username: ''
                };
            }

            $http({
                method: 'POST',
                url: baseUrl + '/rest/api/developer/register',
                data: JSON.stringify(data)
            }).then(function (response) {
                if (response.data instanceof Array) {
                    $scope.errors = processErrors(response.data);
                } else {
                    $scope.errors = {};
                    alert('Your account has been successfully created.\n\nYou may now sign in!');
                    window.location.href = baseUrl + '/public/login';
                }
            });
        };
    }
]);

function processErrors(errors) {
    var temp = {};

    if (isDebug) {
        console.log('process errors');
        console.log(errors);
    }

    for (var index in errors) {
        var error = errors[index];
        temp[error.field] = error.defaultMessage;

        if (isDebug) {
            console.log('error field name [' + error.field + ' - ' + error.defaultMessage + ']');
        }
    }

    return temp;
}