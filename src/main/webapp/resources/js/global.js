var globalModule = angular.module('globalModule', []);

globalModule.config(function ($httpProvider, $provide) {
    console.log('initiliaze global module config!');

    $provide.factory('httpInterceptor', function ($q, $rootScope) {
        return {
            'request': function (config) {
                showBlockUI = config.blockUI;
                // intercept and change config: e.g. change the URL
                // config.url += '?nocache=' + (new Date()).getTime();
                // broadcasting 'httpRequest' event
                $rootScope.$broadcast('httpRequest', config);

                return config || $q.when(config);
            },
            'response': function (response) {
                // we can intercept and change response here...
                // broadcasting 'httpResponse' event
                $rootScope.$broadcast('httpResponse', response);

                return response || $q.when(response);
            },
            'requestError': function (rejection) {
                // broadcasting 'httpRequestError' event
                $rootScope.$broadcast('httpRequestError', rejection);
                return $q.reject(rejection);
            },
            'responseError': function (rejection) {
                alert('Oops! Something went wrong!');

                // broadcasting 'httpResponseError' event
                $rootScope.$broadcast('httpResponseError', rejection);
                return $q.reject(rejection);
            }
        };
    });

    $httpProvider.interceptors.push('httpInterceptor');
});