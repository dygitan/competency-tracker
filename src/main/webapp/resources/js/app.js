/* global angular, baseUrl, csrfToken, moment */

var defaultCoveredPeriodStart;
var defaultCoveredPeriodEnd;

$(function () {
//    var defaultCoveredPeriodStart = moment().subtract(1, 'weeks').startOf('isoWeek');
//    var defaultCoveredPeriodEnd = moment(new Date(defaultCoveredPeriodStart)).add(7, 'days');

    defaultCoveredPeriodStart = moment().startOf('isoWeek');
    defaultCoveredPeriodEnd = moment(new Date(defaultCoveredPeriodStart)).add(4, 'days');

    $('#coveredPeriodStart').datetimepicker({
        format: 'MMM/DD/YYYY',
        defaultDate: defaultCoveredPeriodStart
    });

    $('#coveredPeriodEnd').datetimepicker({
        format: 'MMM/DD/YYYY',
        defaultDate: defaultCoveredPeriodEnd
    });

    $('#coveredPeriodStart').on('dp.change', function (e) {
        $('#coveredPeriodEnd').data('DateTimePicker').date(moment(new Date(e.date)).add(4, 'days'));
    });
});

var app = angular.module('developerModule', [
    'developerCtrl',
    'globalModule'
]);

var developerCtrl = angular.module('developerCtrl', []);

developerCtrl.controller('showAllDevsCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        var defaultCoveredPeriodStart = moment().subtract(1, 'weeks').startOf('isoWeek').format('MMM/DD/YYYY');
        var defaultCoveredPeriodEnd = moment(new Date(defaultCoveredPeriodStart)).add(4, 'days').format('MMM/DD/YYYY');

        console.log(defaultCoveredPeriodStart + ' - ' + defaultCoveredPeriodEnd);

        getAllDevs($scope, $http, defaultCoveredPeriodStart, defaultCoveredPeriodEnd);

        $scope.filterDevs = function () {
            getAllDevs($scope, $http, $('#coveredPeriodStartValue').val(), $('#coveredPeriodEndValue').val());
        };

        $scope.export = function () {
            window.open('export/data?s=' + $('#coveredPeriodStartValue').val() + '&e=' + $('#coveredPeriodEndValue').val(), '_blank');
        };
    }
]);

function getAllDevs($scope, $http, start, end) {
    $scope.items = {};

    $http.get(baseUrl + '/rest/api/developer/get/all/1',
            {
                params: {
                    s: start,
                    e: end
                }
            })
            .then(function (response) {
                $scope.items = response.data;
            });
}

developerCtrl.controller('showAllCategoriesCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        console.log('view items controller!');

        $http.get(baseUrl + '/rest/api/category/all')
                .then(function (response) {
                    $scope.categories = response.data;
                });
    }
]);

developerCtrl.controller('manageCategoryCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        $scope.submit = function () {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            if (!($scope.category.primaryId > 0)) {
                $scope.category = {
                    description: $scope.category.description
                };
            }

            $http({
                method: 'POST',
                url: baseUrl + '/rest/api/category/submit',
                data: JSON.stringify($scope.category)
            }).then(function (response) {
                alert('Record - Success!');
                console.log(response);
            });
        };

    }
]);

developerCtrl.controller('showAllCompetenciesCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        console.log('view items controller!');

        $http.get(baseUrl + '/rest/api/competency/get/all')
                .then(function (response) {
                    $scope.competencies = response.data;
                });
    }
]);

developerCtrl.controller('manageCompetencyCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        $scope.submit = function () {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            if (!($scope.competency.primaryId > 0)) {
                $scope.competency = {
                    description: $scope.competency.description,
                    competencyCategory: {
                        primaryId: $scope.competency.competencyCategory.primaryId
                    }
                };
            }

            $http({
                method: 'POST',
                url: baseUrl + '/rest/api/competency/submit',
                data: JSON.stringify($scope.competency)
            }).then(function (response) {
                alert('Record - Success!');
                console.log(response);
            });
        };

    }
]);

developerCtrl.controller('recordCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        console.log('profile controller!');

        $scope.showCategories = function () {
            $scope.categories = {};

            $http.get(baseUrl + '/rest/api/category/all')
                    .then(function (response) {
                        $scope.categories = response.data;
                    });
        };

        $scope.showCompetencies = function (categoryId) {
            $scope.message = 'Select category first.';

            $scope.competencies = {};
            $scope.devCompetencies = {};

            $http.get(baseUrl + '/rest/api/competency/get/category/' + categoryId)
                    .then(function (response) {
                        $scope.competencies = response.data;
                    });
        };

        $scope.showDevCompetencies = function (competencyId) {
            $scope.competencyId = competencyId;

            if (competencyId) {
                $http.get(baseUrl + '/rest/api/competency/get/developer/' + $scope.developer.primaryId + '/' + competencyId)
                        .then(function (response) {
                            $scope.devCompetencies = response.data;

                            if (response.data.length == 0) {
                                $scope.message = 'No record/s available.';
                            }
                        });

                $('button.record-form-btn').show();
                $('[data-toggle="tooltip"]').tooltip();
            } else {
                $scope.devCompetencies = {};
                $scope.message = 'Select category first.';

                $('button.record-form-btn').hide();
            }
        };

        $scope.showRecordModal = function () {
            if ($scope.devCompetencies.length > 0) {
                if (confirm('Are you sure you want to use your previous week competency rating?')) {
                    $scope.submit($scope.devCompetencies[0]);
                }
            } else {
                alert('No record/s available.');
            }
        };

        $scope.submit = function (temp) {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var coveredPeriodStart = defaultCoveredPeriodStart.format('MMM/DD/YYYY');
            var coveredPeriodEnd = defaultCoveredPeriodEnd.format('MMM/DD/YYYY');
            var rating;

            if (temp) {
                rating = temp.rating;
            } else {
                coveredPeriodStart = $('#coveredPeriodStartValue').val();
                coveredPeriodEnd = $('#coveredPeriodEndValue').val();
                rating = $scope.record.rating;
            }

            var data = {
                competency: {
                    primaryId: $scope.competencyId
                },
                developer: {
                    primaryId: $scope.developer.primaryId
                },
                rating: rating,
                coveredPeriodStart: coveredPeriodStart,
                coveredPeriodEnd: coveredPeriodEnd
            };

            $http({
                method: 'POST',
                url: baseUrl + '/rest/api/developer/competency/submit',
                data: JSON.stringify(data)
            }).then(function (response) {
                $scope.devCompetencies = response.data;
                $('#recordModal').modal('hide');
                alert('Record - Success!');
            });
        };

        $scope.submitA = function () {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var data = new Array();
            var temp = $scope.developer.developerCompetencies;
            var index = 0;

            for (var ctr in temp) {
                if (temp[ctr].rating && temp[ctr].rating > 0) {
                    data[index] = {
                        developer: {
                            primaryId: temp[ctr].developerId
                        },
                        competency: {
                            primaryId: temp[ctr].competencyId
                        },
                        rating: temp[ctr].rating,
                        coveredPeriodStart: $('#coveredPeriodStartValue').val(),
                        coveredPeriodEnd: $('#coveredPeriodEndValue').val()
                    };
                    index++;
                }
            }

            $http({
                method: 'POST',
                url: baseUrl + '/rest/api/developer/competency/submit',
                data: JSON.stringify(data)
            }).then(function (response) {
                alert('Record - Success!');
                console.log(response);
            });
        };
    }
]);