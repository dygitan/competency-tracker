<!DOCTYPE html>
<html>
    <head>      
        <%@include file="common/taglibs.jsp" %>

        <link rel="stylesheet" href="${baseUrl}/resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="${baseUrl}/resources/css/style.css">
        <link rel="stylesheet" href="${baseUrl}/resources/css/login.css">
    </head>
    <body>
        <div class="container">

            <form name="loginForm" class="form-signin" role="form" action="${pageContext.request.contextPath}/public/login" method="POST">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Login 
                    </div>
                    <div class="panel-body">
                        <h2 class="form-signin-heading" style="margin: 0px; padding: 10px 0px; margin-bottom: 5px; font-size: 15px; font-weight: 600">WELCOME</h2>
                        <input type="text" name="username" class="form-control" placeholder="Username" required autofocus="autofocus" />
                        <input type="password" name="password" class="form-control" placeholder="Password" required="required" />

                        <div class="row">
                            <div class="col-md-7 text-left" style="padding: 0px; padding-left: 15px;">
                                <hr style="margin: 5px 0px 5px;"/>
                                <!--<a style="font-size: 11px">Forgot password?</a>-->
                                <!--<br>-->
                                <span style="font-size: 11px">No account yet? </span> <a style="font-size: 11px" href="${baseUrl}/public/register">Register</a>
                            </div>
                            <div class="col-md-5 text-right" style="padding: 0px; padding-right: 15px">
                                <button class="btn btn-success btn-circle" type="submit">
                                    Sign in <span class="glyphicon glyphicon-log-in"></span>
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </div>
            </form>

        </div>
    </body>
</html>
