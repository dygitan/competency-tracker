<div class="container">
    <div class="alert alert-danger alert-dismissible" role="alert">
        
        <strong>RESTRICTED PAGE!</strong> You do not have permission to view this page.
    </div>
</div>