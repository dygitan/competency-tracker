<%-- 
    Document   : global-modal
    Created on : Dec 22, 2014, 11:38:47 AM
    Author     : Patrick Tan
--%>
<%@include file="../common/taglibs.jsp" %>

<div id="promptModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">e-Tracking</h5>
            </div>
            <div class="modal-body">
                <span id="promptMessage"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div id="loaderModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content text-center" style="padding: 8px 0px;
             opacity: .6;
             background-color: #000;
             border: solid 2px white;">
            <span style="color: white; font-size: 15px; font-family: monospace"> 
                PLEASE WAIT <img src="${baseUrl}/resources/img/loader.gif" />
            </span>
        </div>
    </div>
</div>

<div id="blockOverlay" style="display:none;"></div>
<div id="blockPopup" style="display:none;">
    <div class="block-popup">
        <div class="block-popup-container modal-content" ng-controller="popupCtrl">
            <div style="padding: 5px 0px">
                <span class="message"> 
                    Please wait while the system is processing your request
                </span>
                <img src="${baseUrl}/resources/img/loader.gif" style="margin-left: 5px" />
            </div>
            <div class="button-area text-right" style="display: none">
                <hr>
                <button id="btnCloseModal" class="btn btn-sm btn-default" ng-click="close()">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<style>
    #blockOverlay {
        background-color: #000;
        cursor: wait;
        opacity: .85;
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        top: 0;
        z-index: 9999999;
    }

    .block-popup-container {
        /*cursor: wait;*/
        background-image: url('../../../resources/img/bg.png');
        border: solid 1px #d9edf7;  
        box-shadow: 0px 1px 20px 2px #98B3CA;
        -webkit-box-shadow: 0px 1px 20px 2px #98B3CA;
        -moz-box-shadow: 0px 1px 20px 2px #98B3CA;
        margin: auto; 
        padding: 10px;
        width: 425px; 
    }

    .block-popup-container hr {
        border: solid 1px #333;
    }

    @media(max-width: 380px) {
        .block-popup-container {
            border: solid 1px red ! important;
            width: 95%;
        }
    }

    .danger {
        border: solid 1px #AB5959;  
        box-shadow: 0px 1px 20px 2px #DF6A6A;
        -webkit-box-shadow: 0px 1px 20px 2px #DF6A6A;
        -moz-box-shadow: 0px 1px 20px 2px #DF6A6A;

    }

    .danger .message, .danger hr {
        color: #a94442;
        border-color: #a94442;
    }

    .block-popup .message {
        /*cursor: wait;*/
        color: #333;
        font-family: monospace; 
        font-size: 12px;
        font-weight: bold;
    }

    .block-popup {
        color: black;
        /*cursor: wait;*/
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 10000000;
        margin: auto;
        opacity: 1;
    }
</style>

