<%@include file="../common.jsp" %>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${baseUrl}/secured/dashboard">${appName}</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav active">
                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <li>
                        <a href="${baseUrl}/secured/developer/all">Developers</a>
                    </li>
                </security:authorize>
                <security:authorize access="hasRole('ROLE_DEVELOPER')">
                    <li>
                        <a href="${baseUrl}/secured/developer/${user.developerId}">Log Competency</a>
                    </li>
                </security:authorize>
                <security:authorize access="isAnonymous()">
                    <li>
                        <a href="${baseUrl}/public/login">
                            <span class="glyphicon glyphicon-log-in"></span> Login
                        </a>
                    </li>
                </security:authorize>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                        <ul class="dropdown-menu">                            
                            <li>
                                <a href="${baseUrl}/secured/category/all">Categories</a>
                            </li>                            
                            <li>
                                <a href="${baseUrl}/secured/competency/all">Competencies</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="${baseUrl}/secured/developer/export">Export Data</a>
                            </li>
                        </ul>
                    </li>
                </security:authorize>
                <security:authorize access="isAuthenticated()">
                    <li>
                        <a>
                            <span class="glyphicon glyphicon-user"></span> 
                            <strong><security:authentication property="principal.username" /></strong>
                        </a>
                    </li>
                    <li>
                        <a>
                            <form class="form-inline" action="${baseUrl}/logout" method="post" style="margin: 0px; padding: 0px">
                                <span class="glyphicon glyphicon-log-out"></span> <input type="submit" value="Log out" style="border: none; background: none"/>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </form>
                        </a>
                    </li>
                </security:authorize>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>