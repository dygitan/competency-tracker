<%@include file="../common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            ${appName} | <tiles:insertAttribute name="title" ignore="true" />
        </title>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <link rel="stylesheet" href="${baseUrl}/resources/css/bootstrap.min.css" />
        <link rel="stylesheet" href="${baseUrl}/resources/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="${baseUrl}/resources/css/style.css" />

        <script src="${baseUrl}/resources/js/jquery.min.js"></script>
        <script src="${baseUrl}/resources/js/angular.min.js"></script>
        <script src="${baseUrl}/resources/js/bootstrap.min.js"></script>
        <script src="${baseUrl}/resources/js/moment.min.js"></script>
        <script src="${baseUrl}/resources/js/bootstrap-datetimepicker.min.js"></script>
        <script src="${baseUrl}/resources/js/global.js"></script>
        <script src="${baseUrl}/resources/js/app.js"></script>

        <style>
            body, table {
                font-size: 12px;
                font-family: monospace;
            }

            .panel-group .panel {
                border-radius: 0px;
            }
        </style>
    </head>
    <body>
        <tiles:insertAttribute name="navigation" />

        <div class="container-fluid" style="margin-top: 65px">
            <tiles:insertAttribute name="content" />
            <%@include file="global-modal.jsp" %>
        </div>

        <tiles:insertAttribute name="footer" />
    </body>
</html>
