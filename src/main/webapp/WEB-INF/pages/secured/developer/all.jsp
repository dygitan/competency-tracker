<%@include file="../../common.jsp" %>

<style>
    .developer-list-group {
        list-style: none;
        border-top: solid 1px #ddd;                
        margin: 0px;
        padding: 0px;
    }

    .developer-list-group-item {
        border-bottom: 1px solid #ddd;
        border-left: 1px solid #ddd;
        border-right: 1px solid #ddd;
        background: #fff;
        padding: 10px;
    }

    .developer-list-group-item:hover {
        background: #f5f5f5;
    }

    .developer-list-group-item:hover .developer-average-container {
        background: #fff;
    }
</style>

<div class="row" ng-app="developerModule" ng-controller="showAllDevsCtrl">
    <div class="container-fluid" style="padding-bottom: 10px">
        <div class="col-lg-3 col-md-4 col-sm-8">
            <div id="coveredPeriodStart" class="input-group date my-datetimepicker">                                    
                <input id="coveredPeriodStartValue" type="text" class="form-control" ng-model="coveredPeriodStart" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>            
            <div id="coveredPeriodEnd" class="input-group date my-datetimepicker" style="margin-top: 10px">
                <input id="coveredPeriodEndValue" type="text" class="form-control" ng-model="coveredPeriodEnd" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            <div class="text-right" style="margin-top: 10px; padding-bottom: 10px">
                <button class="btn btn-xs btn-default" ng-click="filterDevs()">FILTER</button>
            </div>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-8">
            <div>
                <ul class="developer-list-group">
                    <li class="developer-list-group-item" ng-repeat="item in items">
                        <div class="media-left">
                            <img style="height: 55px; width: 55px; border: solid 1px #e7e7e7" class="media-object" src="${baseUrl}/resources/img/avatar.png" alt="...">
                        </div>
                        <div class="media-body" style="padding-left: 5px; width: 100%">
                            <h4 class="media-heading">{{item.lastName}}, {{item.firstName}}</h4>
                            <div>
                                <hr style="padding: 0px; margin: 5px 0px;">
                                <a href="${baseUrl}/secured/developer/{{item.developerId}}" style="text-decoration: none;">
                                    <button class="btn btn-xs btn-default">Record</button>
                                </a>
                                <a href="${baseUrl}/secured/developer/competency/history/{{item.developerId}}" style="text-decoration: none;">
                                    <button class="btn btn-xs btn-default">View</button>
                                </a>
                            </div>

                        </div>
                        <style>
                            .developer-average-container {
                                background: #f5f5f5; 
                                height: 60px; 
                                width: 60px; 
                                border: solid 1px #e7e7e7;

                            }

                            .developer-average-container strong {
                                font-size: 15px;
                                /*line-height: 65px;*/
                                /*margin: 12px*/
                            }
                        </style>
                        <div class="media-right">
                            <div class="developer-average-container text-center">
                                <label style="margin-top: 5px">RATING</label>
                                <strong>{{(item.rating ? item.rating : 0) | number : 2}}</strong>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>        
    </div>
</div>
