<%@include file="../../common.jsp" %>

<div ng-app="developerModule" ng-controller="recordCtrl">
    <div class="col-lg-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                DEVELOPER
            </div>
            <div class="panel-body">
                <label>${requestScope.developer.lastName}, ${requestScope.developer.firstName}</label>
            </div>
        </div>
    </div>
    <div class="col-lg-7">
        <c:set var="historyMode" value="true" />
        
        <%@include file="competency.jsp" %>
    </div>
</div>