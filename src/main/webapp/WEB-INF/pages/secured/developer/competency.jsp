<%@include file="../../common.jsp" %>

<style>
    .list-group-item strong {
        padding-left: 10px;
    }
</style>
<div class="panel panel-primary">    
    <div class="panel-heading">
        COMPETENCIES
    </div>
    <div class="panel-body">
        <input type="hidden" ng-model="developer.primaryId" ng-init="developer.primaryId = '${requestScope.developer.primaryId}'" />

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" ng-init="showCategories()" style="margin-bottom: 0px;">
            <div class="panel panel-default" ng-repeat="category in categories">
                <div class="panel-heading" role="tab" id="category-{{category.categoryId}}">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{category.primaryId}}" aria-expanded="true" aria-controls="collapseOne" 
                       style="font-weight: bold; color: #262626"
                       ng-click="showCompetencies(category.primaryId)">
                        {{category.description}}
                    </a>
                </div>
                <div id="collapse-{{category.primaryId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="category-{{category.primaryId}}">
                    <div class="panel-body">                        
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group-sm">
                                    <select class="form-control" ng-change="showDevCompetencies(competency.primaryId)" ng-model="competency.primaryId">
                                        <option value="">--- SELECT CATEGORY ---</option>
                                        <option ng-repeat="competency in competencies" 
                                                ng-value="competency.primaryId" 
                                                title=" {{competency.description}}">
                                            {{competency.description}}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <c:if test="${!historyMode}">
                                <div class="col-lg-8 text-right">
                                    <button id="btnNoUpdate" type="button" 
                                            class="btn btn-default btn-xs record-form-btn" 
                                            ng-click="showRecordModal()" 
                                            style="display: none"
                                            data-toggle="tooltip" data-placement="left"
                                            title="Your previous rating will be used as basis of this week rating.">
                                        COPY PREVIOUS RATING
                                    </button>
                                    <button type="button" class="btn btn-default btn-xs record-form-btn" data-toggle="modal" data-target="#recordModal" style="display: none">
                                        RECORD
                                    </button>
                                </div>
                            </c:if>
                        </div>
                        <hr>
                        <div class="container-fluid">
                            <div class="row">
                                <ul class="list-group" style="margin-bottom: 0px">
                                    <li class="list-group-item list-group-item-warning" ng-hide="devCompetencies.length > 0">
                                        {{message}}
                                    </li>
                                    <li class="list-group-item" ng-repeat="devCompetency in devCompetencies">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                Period Start
                                                <br>
                                                <strong>&raquo; {{devCompetency.coveredPeriodStart| date:'MMM/dd/yyyy'}}</strong>
                                            </div>
                                            <div class="col-lg-4">
                                                Period End
                                                <br>
                                                <strong>&raquo; {{devCompetency.coveredPeriodEnd| date:'MMM/dd/yyyy'}}</strong>
                                            </div>
                                            <div class="col-lg-3">
                                                Rating
                                                <br>
                                                <strong>&raquo; {{devCompetency.rating| number : 2}}</strong>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="recordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="recordModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title">RECORD COMPETENCY</h5>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group">
                                <label for="">
                                    Period Covered (START) <span class="required-field">*</span>
                                </label>
                                <div class="input-group-sm">
                                    <div id="coveredPeriodStart" class="input-group date my-datetimepicker">                                    
                                        <input id="coveredPeriodStartValue" type="text" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    <div class="text-right">
                                        <span ng-show="errors.coveredPeriodStart" class="ng-hide validation-error">{{errors.coveredPeriodStart}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    Period Covered (END) <span class="required-field">*</span>
                                </label>
                                <div class="input-group-sm">
                                    <div id="coveredPeriodEnd" class="input-group date my-datetimepicker">                                    
                                        <input id="coveredPeriodEndValue" type="text" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    <div class="text-right">
                                        <span ng-show="errors.coveredPeriodEnd" class="ng-hide validation-error">{{errors.coveredPeriodEnd}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    Rating <span class="required-field">*</span>
                                </label>
                                <div class="input-group-sm">
                                    <input type="text"
                                           class="form-control datetimepicker text-right" 
                                           ng-model="record.rating"
                                           ng-init="record.rating = 0"
                                           />
                                    <div class="text-right">
                                        <span ng-show="errors.rating" class="ng-hide validation-error">{{errors.rating}}</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="button" class="btn btn-default btn-xs" ng-click="submit()">SUBMIT</button>
                                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">CLOSE</button>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>