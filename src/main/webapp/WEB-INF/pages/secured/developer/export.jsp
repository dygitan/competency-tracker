<div ng-app="developerModule" ng-controller="showAllDevsCtrl">
    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                EXPORT DATA
            </div>
            <div class="panel-body">
                <div id="coveredPeriodStart" class="input-group date my-datetimepicker">                                    
                    <input id="coveredPeriodStartValue" type="text" class="form-control" ng-model="coveredPeriodStart" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>            
                <div id="coveredPeriodEnd" class="input-group date my-datetimepicker" style="margin-top: 10px">
                    <input id="coveredPeriodEndValue" type="text" class="form-control" ng-model="coveredPeriodEnd" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <hr>
                <div class="text-right">
                    <button ng-click="export()" class="btn btn-xs btn-default" style="margin-top: 10px">
                        EXPORT DATA
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>