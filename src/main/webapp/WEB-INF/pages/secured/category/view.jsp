<%-- 
    Document   : view
    Created on : Aug 25, 2015, 8:11:33 PM
    Author     : Joseph Patrick Tan
--%>

<div class="row" ng-app="developerModule">
    <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
        <div class="panel panel-primary" ng-controller="manageCategoryCtrl">
            <div class="panel-heading">                    
                Category &raquo; ${category.primaryId > 0 ? 'Edit' : 'New'}
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="input-group-sm">
                        <input type="hidden" 
                               ng-init="category.primaryId = '${category.primaryId}'"
                               />
                        <input type="text" 
                               class="form-control"
                               placeholder="Description"
                               maxlength="45"
                               ng-model="category.description"
                               ng-init="category.description = '${category.description}'"
                               />
                        <div class="text-right">
                            <span ng-show="errors.description" class="ng-hide validation-error">{{errors.description}}</span>
                        </div>
                    </div>
                </div>
                <hr>
                <div>
                    <button class="btn btn-xs btn-success" ng-click="submit()">SUBMIT</button>
                    <a href="all">
                        <button class="btn btn-xs btn-danger">CANCEL</button>
                    </a>
                </div>
            </div>    
        </div>  
    </div> 

</div>

