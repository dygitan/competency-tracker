<%-- 
    Document   : all
    Created on : Aug 25, 2015, 7:41:54 PM
    Author     : Joseph Patrick Tan
--%>

<%@include file="../../common.jsp" %>

<div class="row" ng-app="developerModule">

    <div class="col-lg-4" ng-controller="showAllCategoriesCtrl">
        <a href="new">
            <button class="btn btn-default btn-xs">NEW RECORD</button>
        </a>
        <div class="table-responsive" style="margin-top: 5px">
            <table class="table table-bordered table-condensed table-hover table-striped">
                <thead>
                    <tr>
                        <td>Competency</td>
                        <td>&nbsp;</td>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="category in categories">
                        <td style="width: 70%">{{category.description}}</td>
                        <td class="text-center">
                            <a href="${baseUrl}/secured/category/{{category.primaryId}}">
                                <button class="btn btn-xs btn-info">Edit</button>
                            </a>
<!--                            <a href="${baseUrl}">
                                <button class="btn btn-xs btn-danger">Delete</button>
                            </a>-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
