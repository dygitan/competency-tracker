<%-- 
    Document   : manage
    Created on : Aug 25, 2015, 7:40:39 PM
    Author     : Joseph Patrick Tan
--%>

<%@include file="../../common.jsp" %>

<div class="row" ng-app="developerModule">
    <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
        <div class="panel panel-primary" ng-controller="manageCompetencyCtrl">
            <div class="panel-heading">                    
                Competency &raquo; ${competency.primaryId > 0 ? 'Edit' : 'New'}
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="input-group-sm">
                        <input type="hidden" 
                               ng-init="competency.primaryId = '${competency.primaryId}'"
                               />
                        <select class="form-control"
                                ng-model="competency.competencyCategory.primaryId"
                                ng-init="competency.competencyCategory.primaryId = '${competency.competencyCategory.primaryId}'">
                            <option value="">- Select -</option>
                            <c:forEach var="category" items="${categories}">
                                <option value="${category.primaryId}">${category.description}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group-sm">
                        
                        <input type="text" 
                               class="form-control"
                               placeholder="Description"
                               maxlength="45"
                               ng-model="competency.description"
                               ng-init="competency.description = '${competency.description}'"
                               />
                        <div class="text-right">
                            <span ng-show="errors.description" class="ng-hide validation-error">{{errors.description}}</span>
                        </div>
                    </div>
                </div>
                <hr>
                <div>
                    <button class="btn btn-xs btn-success" ng-click="submit()">SUBMIT</button>
                    <a href="all">
                        <button class="btn btn-xs btn-danger">CANCEL</button>
                    </a>
                </div>
            </div>    
        </div>  
    </div> 

</div>
