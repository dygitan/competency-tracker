<%-- 
    Document   : all
    Created on : Aug 25, 2015, 7:39:59 PM
    Author     : Joseph Patrick Tan
--%>

<%@include file="../../common.jsp" %>

<div class="row" ng-app="developerModule">

    <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12" ng-controller="showAllCompetenciesCtrl">
        <a href="new">
            <button class="btn btn-default btn-xs">NEW RECORD</button>
        </a>
        <div class="table-responsive" style="margin-top: 5px">
            <table class="table table-bordered table-condensed table-hover table-striped">
                <thead>
                    <tr>                        
                        <td>Category</td>
                        <td>Competency</td>                        
                        <td>&nbsp;</td>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="competency in competencies">
                        <td>{{competency.competencyCategory.description}}</td>
                        <td>{{competency.description}}</td>
                        <td class="text-center">
                            <a href="${baseUrl}/secured/competency/{{competency.primaryId}}">
                                <button class="btn btn-xs btn-info">Edit</button>
                            </a>
<!--                            <a href="${baseUrl}">
                                <button class="btn btn-xs btn-danger">Delete</button>
                            </a>-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
