<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<c:set var="req" value="${pageContext.request}" />
<c:set var="baseUrl" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
<c:set var="appName" value="-APPLICATION NAME-" />

<script>
    var csrfToken = '${_csrf.token}';
    var baseUrl = '${pageContext.request.contextPath}';
    var isDebug = false;
</script>

<security:authentication var="user" property="principal" />
