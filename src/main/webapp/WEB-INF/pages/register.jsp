<%@include file="common.jsp" %>

<script src="${baseUrl}/resources/js/global.js"></script>
<script src="${baseUrl}/resources/js/register.js"></script>

<div ng-app="registerModule">
    <div class="row" ng-controller="registerCtrl">
        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
            <div class="panel panel-primary">
                <div class="panel-heading">                    
                    REGISTER | Developer
                </div>
                <div class="panel-body">    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control"
                                           placeholder="Last name"
                                           maxlength="45"
                                           ng-model="developer.lastName"
                                           />
                                    <div class="text-right">
                                        <span ng-show="errors.lastName" class="ng-hide validation-error">{{errors.lastName}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">                                
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="First name"
                                           maxlength="45"
                                           ng-model="developer.firstName"
                                           />
                                    <div class="text-right">
                                        <span ng-show="errors.firstName" class="ng-hide validation-error">{{errors.firstName}}</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">                                
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Username"
                                           maxlength="20"
                                           ng-model="developer.username"
                                           />
                                    <div class="text-right">
                                        <span ng-show="errors.username" class="ng-hide validation-error">{{errors.username}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">                                
                                <div class="input-group-sm">
                                    <input type="password" 
                                           class="form-control" 
                                           placeholder="New password"
                                           maxlength="20"
                                           ng-model="developer.password"
                                           />
                                    <div class="text-right">
                                        <span ng-show="errors.password" class="ng-hide validation-error">{{errors.password}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">                                
                                <div class="input-group-sm">
                                    <input type="password" 
                                           class="form-control" 
                                           placeholder="Confirm password"
                                           maxlength="20"
                                           ng-model="developer.confirmPassword"
                                           />
                                    <div class="text-right">                                        
                                        <span ng-show="!errors['developerRegistration.password'] && errors['developerRegistration.valid']" class="ng-hide validation-error">{{errors['developerRegistration.valid']}}</span>
                                        <span ng-show="errors.valid && !errors.password" class="ng-hide validation-error">
                                            {{errors.valid}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success btn-xs" ng-click="register()">REGISTER</button>
                            |
                            <button class="btn btn-danger btn-xs" ng-click="reset()">RESET</button>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
