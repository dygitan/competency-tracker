CREATE DATABASE  IF NOT EXISTS `skills_matrix` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `skills_matrix`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: skills_matrix
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `competency`
--

DROP TABLE IF EXISTS `competency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competency` (
  `competency_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`competency_id`),
  KEY `fk_skill_category_id_idx` (`category_id`),
  CONSTRAINT `fk_compentency_category_id` FOREIGN KEY (`category_id`) REFERENCES `competency_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competency`
--

LOCK TABLES `competency` WRITE;
/*!40000 ALTER TABLE `competency` DISABLE KEYS */;
INSERT INTO `competency` VALUES (1,1,'AB Java Stream I/O'),(2,1,'AA Java Exception'),(3,1,'Inheritance - instance of'),(4,1,'Overriding/Polymorphism'),(5,1,'Abstraction/Encapsulation'),(6,1,'Package and Classpath'),(7,1,'Java Documentation'),(8,1,'Data Structure'),(9,1,'Multithreading'),(10,1,'Serialization'),(11,1,'Generics'),(12,2,'RESTful Web Services');
/*!40000 ALTER TABLE `competency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competency_category`
--

DROP TABLE IF EXISTS `competency_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competency_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competency_category`
--

LOCK TABLES `competency_category` WRITE;
/*!40000 ALTER TABLE `competency_category` DISABLE KEYS */;
INSERT INTO `competency_category` VALUES (1,'Java Core'),(2,'Spring'),(3,'Hibernate'),(4,'RESTful WS'),(5,'Big Data'),(6,'asd'),(7,'Hello World');
/*!40000 ALTER TABLE `competency_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `developer`
--

DROP TABLE IF EXISTS `developer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `developer` (
  `developer_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`developer_id`),
  KEY `fk_developer_username_idx` (`username`),
  CONSTRAINT `fk_developer_username` FOREIGN KEY (`username`) REFERENCES `sec_user` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `developer`
--

LOCK TABLES `developer` WRITE;
/*!40000 ALTER TABLE `developer` DISABLE KEYS */;
INSERT INTO `developer` VALUES (1,'Tan','Patrick','Miranda',NULL),(2,'He','skdj','asskdl',NULL),(3,'kajsd','kljslkdl','kjlsd',NULL),(4,'Hello','World','World','patchi'),(5,'Rizal','Jose','Jose','jrizal'),(6,'Gamboa','Jeanette','Jeanette','jgamboa');
/*!40000 ALTER TABLE `developer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `developer_competency`
--

DROP TABLE IF EXISTS `developer_competency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `developer_competency` (
  `developer_competency_id` int(11) NOT NULL AUTO_INCREMENT,
  `developer_id` int(11) NOT NULL,
  `competency_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `covered_period_start` date DEFAULT NULL,
  `covered_period_end` date DEFAULT NULL,
  PRIMARY KEY (`developer_competency_id`),
  KEY `fk_dev_skill_dev_id_idx` (`developer_id`),
  KEY `fk_dev_skill_skill_id_idx` (`competency_id`),
  CONSTRAINT `fk_dev_comp_comp_id` FOREIGN KEY (`competency_id`) REFERENCES `competency` (`competency_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dev_comp_dev_id` FOREIGN KEY (`developer_id`) REFERENCES `developer` (`developer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `developer_competency`
--

LOCK TABLES `developer_competency` WRITE;
/*!40000 ALTER TABLE `developer_competency` DISABLE KEYS */;
INSERT INTO `developer_competency` VALUES (1,1,1,10,'2015-08-16','2015-08-23'),(2,1,1,15,'2015-08-17','2015-08-23'),(3,1,1,20,'2015-08-18','2015-08-23'),(4,1,2,10,'2015-08-16','2015-08-23'),(5,1,5,10,'2015-08-16','2015-08-23'),(6,1,6,10,'2015-08-16','2015-08-23'),(7,1,7,10,'2015-08-16','2015-08-23'),(8,1,8,10,'2015-08-16','2015-08-23'),(9,1,9,10,'2015-08-16','2015-08-23'),(10,1,10,10,'2015-08-16','2015-08-23'),(11,1,11,10,'2015-08-16','2015-08-23'),(12,1,10,90,'2015-08-16','2015-08-23'),(13,1,2,15,'2015-08-17','2015-08-24'),(14,1,2,80,'2015-08-18','2015-08-25'),(15,5,2,12,'2015-08-05','2015-08-12');
/*!40000 ALTER TABLE `developer_competency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_DEVELOPER'),(3,'ROLE_BIDDER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sec_user`
--

DROP TABLE IF EXISTS `sec_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sec_user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sec_user`
--

LOCK TABLES `sec_user` WRITE;
/*!40000 ALTER TABLE `sec_user` DISABLE KEYS */;
INSERT INTO `sec_user` VALUES ('admin','$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y',1),('auctioneer','$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y',1),('bidder','$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y',1),('jgamboa','$2a$10$h826oFButFoKBoKBch3Y6e3jkezrFt/uJt65slGFK2oRCbSLr3t9S',1),('jrizal','$2a$10$.U3WlJU.jtSaKyaAR0Thc.edH7PW8xn2KHUF3MHQBwhnpatmzsj4i',1),('patchi','$2a$10$hVJ1PWgEjFNMicdclK3iJOUihHhCrc6ULZU1Kq9VvOza1lqqzD7Fm',1);
/*!40000 ALTER TABLE `sec_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sec_user_role`
--

DROP TABLE IF EXISTS `sec_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sec_user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  KEY `fk_user_role_role_id_idx` (`role_id`),
  KEY `fk_user_role_username_idx` (`username`),
  CONSTRAINT `fk_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_role_username` FOREIGN KEY (`username`) REFERENCES `sec_user` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sec_user_role`
--

LOCK TABLES `sec_user_role` WRITE;
/*!40000 ALTER TABLE `sec_user_role` DISABLE KEYS */;
INSERT INTO `sec_user_role` VALUES (1,'admin',1),(2,'patchi',2),(3,'bidder',3),(4,'jrizal',2),(5,'jgamboa',2);
/*!40000 ALTER TABLE `sec_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-27 10:01:03
